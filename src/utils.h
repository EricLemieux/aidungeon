#pragma once

#include <stdlib.h>

int RandInt(const int min, const int max);

template< class T > bool BoxBoxCollision(T b1x, T b1y, T b1w, T b1h, T b2x, T b2y, T b2w, T b2h);