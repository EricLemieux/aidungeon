#pragma once

#include "player.h"

class User : public Player{
public:
	User(const int width, const int height):Player(width,height){}

	virtual void Update(int dx, int dy, int** fullWorld);
};