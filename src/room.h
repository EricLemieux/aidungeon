#pragma once

struct Room{
	Room(){}
	Room(int x, int y, int width, int height){
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	}
	int x;
	int y;
	int width;
	int height;
};