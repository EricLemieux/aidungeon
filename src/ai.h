#pragma once

#include "player.h"
#include <vector>
#include <stdlib.h>
#include <math.h>	

struct Quadrent{
	int value;
	int idX;
	int idY;
};



class AI : public Player{
public:
	AI(const int width, const int height):Player(width,height){
		failCount = new int*[heightQuadrents];
		for(int i = 0; i < heightQuadrents; ++i){
			failCount[i] = new int[widthQuadrents]{0};
		}
	}

	virtual void Update(int** fullWorld, bool canMove=true);

	Quadrent SelectBestQuadrent();
	Tile SelectBestTileFromQuadrent(Quadrent destinationQuadrent);

	int targetX, targetY;

	const int widthQuadrents = width/10;
	const int heightQuadrents  = height/10;

	const int heightStride = height/heightQuadrents;
	const int widthStride = width/widthQuadrents;

	Tile Move(Tile start, Tile end);

	int **failCount;
	Quadrent targetQuad;
};