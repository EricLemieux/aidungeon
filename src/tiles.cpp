#include "tiles.h"

int GetTileValue(int type){
	switch(type){
		case STONE:
			return -99;
			break;
		case EMPTY:
			return 1;
			break;
		case UNKNOWN:
			return 2;
			break;
		case EXIT:
			return 999;
			break;
		default:
			return 0;
			break;
	}
}